package com.example.quierodormir.Chat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quierodormir.R;

public class Chat_Activity extends AppCompatActivity {

    private Button btn_enviar;
    private TextView txt_mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_);

        btn_enviar = findViewById(R.id.btn_enviar);
        txt_mensaje =  findViewById(R.id.txt_mensaje);
        btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            txt_mensaje.setText("");
            }
        });

    }
}
