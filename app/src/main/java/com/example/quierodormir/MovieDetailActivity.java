package com.example.quierodormir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.quierodormir.Chat.Chat_Activity;

public class MovieDetailActivity extends AppCompatActivity {

    private ImageView Movie;
    private Button btn_chat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        btn_chat = findViewById(R.id.btn_chat);


        String  movie = getIntent().getExtras().getString("title");
        int image = getIntent().getExtras().getInt("imgURL");
        Movie = findViewById(R.id.detail_movie_img);
        Movie.setImageResource(image);

        btn_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MovieDetailActivity.this, Chat_Activity.class);
                startActivity(intent);
               Toast.makeText(MovieDetailActivity.this,"hi",Toast.LENGTH_LONG).show();
            }
        });


    }
}
